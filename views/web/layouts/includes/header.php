<?php require_once('core/Auth.php'); ?>
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->

<!-- Header -->
<header class="">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="index.html">
                <h2>Sixteen <em>Clothing</em></h2>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo url('homepage') ?>">Trang chủ
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo url('product/index') ?>">Our Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About Us</a>
                    </li>
                    <li class="nav-item">
                        <?php if (Auth::loggedIn('user')) { ?>
                            <a class="nav-link"><?php echo Auth::getUser('user')['email']; ?><a href="<?php echo url('auth/logout') ?>">Logout</a></a>

                        <?php } else { ?>
                            <a class="nav-link" href="<?php echo url('auth/login') ?>">Đăng nhập</a>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>