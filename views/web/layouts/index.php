<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title><?php defineblock('title') ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo asset('assets/web/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!--

TemplateMo 546 Sixteen Clothing

https://templatemo.com/tm-546-sixteen-clothing

-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/fontawesome.css') ?>">
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/templatemo-sixteen.css') ?>">
    <link rel="stylesheet" href="<?php echo asset('assets/web/assets/css/owl.css') ?>">

</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <?php include('views/web/layouts/includes/header.php') ?>

    <!-- Page Content -->
    <?php defineblock('content') ?>


    <?php include('views/web/layouts/includes/footer.php') ?>



    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo asset('assets/web/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo asset('assets/web/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>


    <!-- Additional Scripts -->
    <script src="<?php echo asset('assets/web/assets/js/custom.js') ?>"></script>
    <script src="<?php echo asset('assets/web/assets/js/owl.js') ?>"></script>
    <script src="<?php echo asset('assets/web/assets/js/slick.js') ?>"></script>
    <script src="<?php echo asset('assets/web/assets/js/isotope.js') ?>"></script>
    <script src="<?php echo asset('assets/web/assets/js/accordions.js') ?>"></script>


</body>

</html>