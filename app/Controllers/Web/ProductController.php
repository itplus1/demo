<?php

require_once('app/Controllers/Web/WebController.php');

class ProductController extends WebController
{
    public function index()
    {
        return $this->view('product/index.php');
    }
}
